package com.jplus.orm.mybatis.plugin;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({ "rawtypes", "serial" })
public class JPage implements Serializable {
	private int DEF_PAGE_VIEW_SIZE = 10;

	/** 当前页 */
	private int page;
	/** 每页显示记录条数 */
	private int pageSize;
	/** 取得查询总记录数 */
	private int count;
	/** 总页数 */
	private int allPage;
	/** 返回结果集 */
	private List data = Collections.emptyList();

	public JPage() {
		this.page = 1;
		this.pageSize = DEF_PAGE_VIEW_SIZE;
	}

	/**
	 * 根据当前显示页与每页显示记录数设置查询信息初始对象
	 */
	public JPage(int page, int pageSize) {
		this.page = (page <= 0) ? 1 : page;
		this.pageSize = (pageSize <= 0) ? DEF_PAGE_VIEW_SIZE : pageSize;
	}

	/**
	 * 取得当前显示页号
	 */
	public int getPage() {
		return (page <= 0) ? 1 : page;
	}

	/**
	 * 设置当前页
	 */
	public void setPage(int page) {
		this.page = page <= 0 ? 1 : page;
	}

	/**
	 * 取得当前显示页号最多显示条数
	 */
	public int getPageSize() {
		return (pageSize <= 0) ? DEF_PAGE_VIEW_SIZE : pageSize;
	}

	/**
	 * 设置当前页显示记录条数
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 取得查询取得记录总数
	 */
	public int getCount() {
		return count;
	}

	/**
	 * 设置查询取得记录总数
	 */
	public void setCount(int count) {
		this.count = (count < 0) ? 0 : count;
		this.allPage = getPages();
	}

	/**
	 * 取得当前查询总页数
	 */
	public int getPages() {
		return (count + getPageSize() - 1) / getPageSize();
	}

	/**
	 * 取得起始显示记录号
	 */
	public int getStartNo() {
		return ((getPage() - 1) * getPageSize() + 1);
	}

	/**
	 * 取得结束显示记录号
	 */
	public int getEndNo() {
		return Math.min(getPage() * getPageSize(), count);
	}

	/**
	 * 取得前一显示页码
	 */
	public int getPrePageNo() {
		return Math.max(getPage() - 1, 1);
	}

	/**
	 * 取得后一显示页码
	 */
	public int getNextPageNo() {
		return Math.min(getPage() + 1, getPages());
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public int getAllPage() {
		return allPage;
	}

	public void setAllPage(int allPage) {
		this.allPage = allPage;
	}

}