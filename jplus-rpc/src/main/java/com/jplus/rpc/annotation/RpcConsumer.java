package com.jplus.rpc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;
import com.jplus.rpc.impl.handler.ServiceType;

/**
 * RPC 服务消费者
 * @author Yuanqy
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Component
// 表明可被 框架 扫描
public @interface RpcConsumer {

	/**
	 * 协议类型
	 */
	ServiceType serviceType() default ServiceType.Netty;

	/**
	 * 服务版本号
	 */
	String version() default "1.0.0";

}
