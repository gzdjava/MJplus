package com.jplus.rpc.bean;

import java.io.Serializable;

import com.jplus.rpc.impl.handler.ServiceType;

/**
 * RPC 请求参数
 * 
 * @author Yuanqy
 *
 */
public class RpcRequest implements Serializable {

	private static final long serialVersionUID = -707895616805221772L;
	private String requestId; // 流水ID
	private Class<?> iface; // 接口
	private String methodName; // 方法名
	private Class<?> methodResult;// 返回类型
	private Class<?>[] parameterTypes;// 入参类型
	private Object[] parameters;// 入参对象
	private ServiceType serviceType;// 服务类型
	private String version;// 版本号

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Class<?> getIface() {
		return iface;
	}

	public void setIface(Class<?> iface) {
		this.iface = iface;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Class<?> getMethodResult() {
		return methodResult;
	}

	public void setMethodResult(Class<?> methodResult) {
		this.methodResult = methodResult;
	}

	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}

	public void setParameterTypes(Class<?>[] parameterTypes) {
		this.parameterTypes = parameterTypes;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}