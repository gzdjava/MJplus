package com.jplus.rpc.bean;

import com.jplus.rpc.annotation.RpcProvider;

/**
 * 服务实现
 * 
 * @author Yuanqy
 *
 */
public class Service {

	private Class<?> impl;// 服务实现类,具体实现会从bean容器获取
	private Class<?> iface;// 服务接口类
	private RpcProvider rpcProvider;// 服务注解

	public Service(Class<?> iface, Class<?> impl, RpcProvider rpcProvider) {
		super();
		this.iface = iface;
		this.impl = impl;
		this.rpcProvider = rpcProvider;
	}

	public Class<?> getIface() {
		return iface;
	}

	public void setIface(Class<?> iface) {
		this.iface = iface;
	}

	public Class<?> getImpl() {
		return impl;
	}

	public void setImpl(Class<?> impl) {
		this.impl = impl;
	}

	public RpcProvider getRpcProvider() {
		return rpcProvider;
	}

	public void setRpcProvider(RpcProvider rpcProvider) {
		this.rpcProvider = rpcProvider;
	}

}
