package com.jplus.rpc.impl.handler.netty.coder;

import com.jplus.utill.serializer.SerializationUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

@SuppressWarnings("rawtypes")
public class NettyEncoder extends MessageToByteEncoder {

	@Override
	public void encode(ChannelHandlerContext ctx, Object in, ByteBuf out) throws Exception {
		byte[] data = SerializationUtils.serialize(in);
		out.writeInt(data.length);
		out.writeBytes(data);
	}
}