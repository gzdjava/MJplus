package com.jplus.rpc.impl.handler;

import com.jplus.rpc.impl.handler.http.HttpService;
import com.jplus.rpc.impl.handler.netty.NettyService;
import com.jplus.rpc.impl.handler.scoket.ScoketService;
import com.jplus.rpc.impl.iface.IAgreement;
/**
 * 通信类型
 * @author Yuanqy
 *
 */
public enum ServiceType {
	Netty("Netty", NettyService.class), 
	Scoket("Scoket", ScoketService.class),
	Http("Http",HttpService.class);

	String name;
	Class<? extends IAgreement> cls;

	private ServiceType(String name, Class<? extends IAgreement> cls) {
		this.name = name;
		this.cls = cls;
	}

	public String getName() {
		return name;
	}

	public Class<? extends IAgreement> getCls() {
		return cls;
	}

}