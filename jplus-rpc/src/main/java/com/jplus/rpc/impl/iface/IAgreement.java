package com.jplus.rpc.impl.iface;

import java.util.Map;

import com.jplus.rpc.bean.RpcRequest;
import com.jplus.rpc.bean.RpcResponse;
import com.jplus.rpc.bean.Service;
import com.jplus.rpc.bean.ZNode;
/**
 * 服务接口
 * @author Yuanqy
 *
 */
public interface IAgreement {

	/**
	 * 服务端发布服务
	 * 
	 * @param watcher
	 * @param handler
	 * @param host
	 * @param port
	 * @throws Exception
	 */
	void opServer(IWatcher watcher, Map<String, Service> handler) throws Exception;

	/**
	 * 客户端调用服务
	 * 
	 * @param request
	 * @param host
	 * @param port
	 * @return
	 * @throws Exception
	 */
	RpcResponse opClient(RpcRequest request, ZNode serNode) throws Exception;

}
