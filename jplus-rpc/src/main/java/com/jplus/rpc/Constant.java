package com.jplus.rpc;

import com.jplus.core.AppConstant;
import com.jplus.core.core.ConfigHandle;
import com.jplus.rpc.impl.handler.ServiceType;
/**
 * RPC公共常量区
 * @author Yuanqy
 *
 */
public class Constant {
	// 使用 ZooKeeper 客户端命令行创建/JPLUS_RPC永久节点，用于存放所有的服务临时节点。
	public static final String ZK_REGISTRY_PATH = ConfigHandle.getString("rpc.registry.root", "/JPLUS_RPC");
	public static final int ZK_SESSION_TIMEOUT = ConfigHandle.getInt("rpc.registry.timeout", 5000);
	public static final String ZK_ADDRESS = ConfigHandle.getString("rpc.registry.address", "127.0.0.1:2181");

	public static final int RPC_SERVER_TIMEOUT = ConfigHandle.getInt("rpc.server.timeout", 5000);
	public static final int RPC_CLIENT_TIMEOUT = ConfigHandle.getInt("rpc.client.timeout", 5000);

	public static final String RPC_LOCAL_HOST = ConfigHandle.getString("rpc.local.host", AppConstant.CONFIG.LocalIP.getValue());

	public static final String PROVIDERS = "providers";
	public static final String CONSUMERS = "consumers";

	public static final int NETTY_PORT = ConfigHandle.getInt("rpc.netty.port", 5120);
	public static final int SCOKET_PORT = ConfigHandle.getInt("rpc.scoket.port", 5121);
	public static final int HTTP_PORT = ConfigHandle.getInt("rpc.http.port", 8080);

	public static int getHost(ServiceType st) {
		switch (st.getName()) {
		case "Netty":
			return NETTY_PORT;
		case "Scoket":
			return SCOKET_PORT;
		case "Http":
			return HTTP_PORT;
		default:
			throw new NullPointerException(st.getName());
		}
	}
}
