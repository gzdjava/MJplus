package com.jplus.core;

import java.util.ArrayList;
import java.util.List;

import com.jplus.core.aop.AopHandle;
import com.jplus.core.bean.BeanHandle;
import com.jplus.core.core.ConfigHandle;
import com.jplus.core.ioc.IocHandle;
import com.jplus.core.mvc.ActionHandle;
import com.jplus.core.mvc.WebUtil;
import com.jplus.core.plugin.Plugin;
import com.jplus.core.plugin.PluginHandle;
import com.jplus.core.util.PathUtil;

/**
 * 系统配置区
 * 
 * @author Yuanqy
 *
 */
public interface AppConstant {

	/**
	 * 系统配置项
	 */
	public enum CONFIG {
		DevelopmentMode("development.mode", "false", "开发者模式"), 
		AppViewSuffix("app.view.suffix", ".jsp", "项目页面类型"), 
		AppViewPath("app.view.path", "/WEB-INF/jsp/", "项目页面目录"), 
		AppHomePage("app.home.page", "/index.jsp", "项目首页地址"), 
		AppIgnorePath("app.ignore.path", "/resources/", "请求忽略目录，放静态资源的。"), 
		AppScanPKG("app.scan.pkg", "", "应用业务代码扫描包名"), 
		AppProp("app.prop",ConfigHandle.getPropPath(), "jweb配置文件"),

		AppJwebPKG("app.jplus.pkg", "com.jplus", "框架包名[不要修改]"), 
		AppName("app.name", PathUtil.getProjectName(), "当前项目名称"), 
		LocalIP("app.local.ip", WebUtil.getLocalRealIp(), "项目当前IP地址");

		String key;
		String value;
		String remark;

		private CONFIG(String key, String value, String remark) {
			this.key = key;
			this.value = value;
			this.remark = remark;
		}

		public static void refresh(String propPath) {
			ConfigHandle.init(propPath);
			for (CONFIG con : CONFIG.values()) {
				con.value = ConfigHandle.getString(con.key, con.value);
			}
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 加载handle顺序
	 */
	@SuppressWarnings("serial")
	List<Class<?>> APP_LOAD_HANDLE = new ArrayList<Class<?>>() {
		{// 严格顺序
			this.add(BeanHandle.class);
			this.add(ActionHandle.class);
			this.add(PluginHandle.class);
			this.add(AopHandle.class);
			this.add(IocHandle.class);
		}
	};
	/**
	 * 加载plugin顺序
	 */
	public List<Class<? extends Plugin>> APP_LOAD_PLUGIN = new ArrayList<Class<? extends Plugin>>();

}
