package com.jplus.core.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;

/**
 * 定义 Service 类
 *
 * @author huangyong
 * @author Yuanqy
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
// 用于被框架扫描
public @interface Service {
	String value() default "";
}
