package com.jplus.core.mvc.handler.param;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.mvc.action.Handler;

/**
 * 入参观察者,并非拦截器，观察者只有action调用触发<br>
 * 用于弥补对于入参的不足， 在这里可以定义任意类型的入参。
 * 
 * @author Yuanqy
 */
public class ParamWatcher {

	private final static Logger logger = LoggerFactory.getLogger(ParamWatcher.class);
	private static List<IWatcher> Ilist = new ArrayList<IWatcher>();

	public static List<Object> doWatcher(Handler handler) throws Exception {
		List<Object> list;
		for (IWatcher i : Ilist) {
			list = i.getParams(handler);
			if (list != null)
				return list;
		}
		return null;
	}

	public static boolean addWatcher(IWatcher watch) {
		logger.info("[Watcher] add a new Watcher:{}", watch.getClass().getName());
		return Ilist.add(watch);
	}
}
