package com.jplus.core.mvc.action;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 封装 Action 方法相关信息
 *
 * @author huangyong
 */
public class Handler {

	private Class<?> actionClass;
	private Method actionMethod;
	private String actionPath;

	private String requestPath;
	private Map<String, String> requestRestful;

	public Handler(Class<?> actionClass, Method actionMethod, String actionPath) {
		this.actionClass = actionClass;
		this.actionMethod = actionMethod;
		this.actionPath = actionPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

	public Class<?> getActionClass() {
		return actionClass;
	}

	public Method getActionMethod() {
		return actionMethod;
	}

	public String getRequestPath() {
		return requestPath;
	}

	public String getActionPath() {
		return actionPath;
	}

	public Map<String, String> getRequestRestful() {
		return requestRestful;
	}

	public void setRequestRestful(Map<String, String> requestRestful) {
		this.requestRestful = requestRestful;
	}

}