package com.jplus.core.mvc.bean;

import java.io.Serializable;

/**
 * 封装返回数据
 *
 * @author yuanqy
 * @since 1.0
 */
public class Result implements Serializable {
	private static final long serialVersionUID = 1L;

	private final int OK = 1;
	private final int FAIL = -1;
	private final int PROCE = 0;
	private final int ERROR = -1;

	private int code;// 请求返回状态 1 为成功 -1失败 0 处理中 -2异常
	private Object data; // 返回值。

	/**
	 * 默认成功
	 */
	public Result() {
		OK();
	}

	public Result OK() {
		return CODE(OK);
	}

	public Result FAIL() {
		return CODE(FAIL);
	}

	public Result PROCE() {
		return CODE(PROCE);
	}

	public Result ERROR() {
		return CODE(ERROR);
	}

	private Result CODE(int code) {
		this.code = code;
		return this;
	}

	public Result DATA(Object obj) {
		this.data = obj;
		return this;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Boolean IsOK() {
		return code == OK;
	}

	@Override
	public String toString() {
		return "Result [code=" + code + ", data=" + data + "]";
	}
}
