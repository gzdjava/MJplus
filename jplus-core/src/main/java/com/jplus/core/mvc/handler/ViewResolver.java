package com.jplus.core.mvc.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jplus.core.AppConstant;
import com.jplus.core.mvc.DataContext;
import com.jplus.core.mvc.WebUtil;
import com.jplus.core.mvc.bean.Result;
import com.jplus.core.mvc.bean.View;
import com.jplus.core.util.FormatUtil;

/**
 * 默认视图解析器[单例]
 *
 * @author huangyong
 * @author Yuanqy
 */
public class ViewResolver {

	public void resolveView(Object actionMethodResult) {
		if (actionMethodResult != null) {
			// Action 返回值可为 View 或 Result
			if (actionMethodResult instanceof View) {
				// 若为 View，则需考虑两种视图类型（重定向 或 转发）
				View view = (View) actionMethodResult;
				if (FormatUtil.isEmpty(view.getPath()))// 设置默认值[默认就是请求url]
					view.setPath(WebUtil.getRequestPath());
				if (view.isRedirect()) {
					// 获取路径
					String path = view.getPath();
					// 重定向请求
					WebUtil.redirectRequest(path);
				} else {
					// 获取路径
					String path = AppConstant.CONFIG.AppViewPath.getValue() + view.getPath();
					if (!path.endsWith(AppConstant.CONFIG.AppViewSuffix.getValue()))
						path += AppConstant.CONFIG.AppViewSuffix.getValue();
					// 初始化请求属性
					Map<String, Object> data = view.getData();
					if (data != null && data.size() > 0) {
						HttpServletRequest request = DataContext.getRequest();
						for (Map.Entry<String, Object> entry : data.entrySet())
							request.setAttribute(entry.getKey(), entry.getValue());
					}
					// 转发请求
					WebUtil.forwardRequest(path);
				}
			} else if (actionMethodResult instanceof Result) {
				Result result = (Result) actionMethodResult;
				WebUtil.writeJSON(result);
			} else {
				// 若为其他，，统统转json
				WebUtil.writeJSON(actionMethodResult);
			}
		}
	}
}
