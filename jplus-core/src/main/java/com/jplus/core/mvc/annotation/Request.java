package com.jplus.core.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;

/**
 * 定义请求
 *
 * @author huangyong
 * @author Yuanqy
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
// 用于被框架扫描
public @interface Request {

	String value();

	/**
	 * 定义公共头请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface All {

		String value();
	}

	/**
	 * 定义 GET 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Get {

		String value();
	}

	/**
	 * 定义 POST 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Post {

		String value();
	}

	/**
	 * 定义 PUT 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Put {

		String value();
	}

	/**
	 * 定义 DELETE 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Delete {
		String value();
	}

	/**
	 * 定义 请求 前缀
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Prefix {
	}

	/**
	 * 定义 请求 后缀
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Suffix {
	}
}
