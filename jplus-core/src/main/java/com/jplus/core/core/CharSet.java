package com.jplus.core.core;

/**
 * 编码列表
 * 
 * @author Yuanqy
 *
 */
public class CharSet {
	public static final String Default = "UTF-8";
	public static final String UTF8 = "UTF-8";
	public static final String GBK = "GBK";
	public static final String GB2312 = "GB2312";
	public static final String ISO88591 = "ISO-8859-1";
}
