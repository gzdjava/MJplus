package com.jplus.core.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.util.FormatUtil;
import com.jplus.core.util.PathUtil;
import com.jplus.core.util.PropsUtil;

/**
 * 配置信息handle
 * 
 * @author Yuanqy
 *
 */
public class ConfigHandle {
	static final Logger logger = LoggerFactory.getLogger(ConfigHandle.class);

	/**
	 * 属性文件对象
	 */
	private static Properties configProps;
	private static String defPropFile = "classpath:app.properties";

	public static void init() {
		init(defPropFile);
	}

	public static void init(String propPath) {
		if (configProps == null || configProps.size() == 0) {
			defPropFile = propPath;
			configProps = loadPropertyFile();
		}
	}

	public static String getPropPath() {
		return defPropFile;
	}

	public static void setProp(String key, String value) {
		configProps.setProperty(key, value);
	}

	public static Properties getProp() {
		return configProps;
	}

	/**
	 * 获取 String 类型的属性值<br>
	 * 如果配置值为空，则返回 key 值<br>
	 * key值格式为：1."xxx" 2."${xxx}" <br>
	 * 没有值报空指针异常
	 */
	public static String getString(String key) {
		String val = PropsUtil.getString(configProps, key);
		if (key.startsWith("${") && key.endsWith("}"))
			val = PropsUtil.getString(configProps, key.substring(2, key.length() - 1));
		if (FormatUtil.isEmpty(val))
			throw new NullPointerException("Prop value is empty, please check!! [key=" + key + "]");
		return val;
	}

	/**
	 * 获取 String 类型的属性值<br>
	 * 如果配置值为空，则返回 defaultValue 值<br>
	 * key值格式为：1."xxx" 2."${xxx}" <br>
	 */
	public static String getString(String key, String defaultValue) {
		String val = PropsUtil.getString(configProps, key);
		if (key.startsWith("${") && key.endsWith("}"))
			val = PropsUtil.getString(configProps, key.substring(2, key.length() - 1));
		if (FormatUtil.isEmpty(val))
			return defaultValue;
		return val;
	}

	/**
	 * 获取 int 类型的属性值
	 */
	public static int getInt(String key) {
		return PropsUtil.getNumber(configProps, key);
	}

	/**
	 * 获取 int 类型的属性值（可指定默认值）
	 */
	public static int getInt(String key, int defaultValue) {
		return PropsUtil.getNumber(configProps, key, defaultValue);
	}

	/**
	 * 获取 boolean 类型的属性值
	 */
	public static boolean getBoolean(String key) {
		return PropsUtil.getBoolean(configProps, key);
	}

	/**
	 * 获取 int 类型的属性值（可指定默认值）
	 */
	public static boolean getBoolean(String key, boolean defaultValue) {
		return PropsUtil.getBoolean(configProps, key, defaultValue);
	}

	/**
	 * 获取指定前缀的相关属性
	 */
	public static Map<String, Object> getMap(String prefix) {
		return PropsUtil.getMap(configProps, prefix);
	}

	/**
	 * 加载配置文件app.properties
	 */
	private static Properties loadPropertyFile() {
		Properties prop = new Properties();
		if (FormatUtil.isEmpty(defPropFile)) {
			logger.info("[警告]未设置配置文件：propertyFile");
			return prop;
		}
		defPropFile = FormatUtil.trimAll(defPropFile);
		String[] fs = defPropFile.split(";");
		for (int i = 0; i < fs.length; i++) {
			String file = fs[i];
			int index = file.indexOf(":");
			String propPath = "";
			if (index < 0)
				propPath = PathUtil.getWebRootPath() + "/" + ((file.charAt(0) == '/') || (file.charAt(0) == '\\') ? "WEB-INF" : "") + file;
			else if (file.substring(0, index).equalsIgnoreCase("classpath"))
				propPath = PathUtil.getRootClassPath() + "/" + file.substring(index + 1, file.length());
			else if (file.substring(0, index).equalsIgnoreCase("file")) {
				propPath = "/" + file.substring(index + 1, file.length());
			}
			propPath = FormatUtil.formatPath(propPath);
			try {
				logger.info(">>load file:{}", propPath);
				InputStream is = new FileInputStream(new File(propPath));
				Properties temp = new Properties();
				temp.load(is);
				prop.putAll(temp);
				logger.debug("propertyFile[{}]:{}", i + 1, propPath);
				for (Entry<Object, Object> entry : prop.entrySet()) {
					loggerDev(logger, "\t{}={}", entry.getKey(), entry.getValue());
				}
			} catch (FileNotFoundException e) {
				logger.error("PropertyFile is not fount", e);
			} catch (IOException e) {
				logger.error("Read IO is Exception", e);
			}
		}
		return prop;
	}

	private static void loggerDev(Logger logger, String format, Object... arguments) {
		// if
		// (FormatUtil.toBoolean(AppConstant.CONFIG.DevelopmentMode.getValue()))
		logger.info(format, arguments);
	}
}
