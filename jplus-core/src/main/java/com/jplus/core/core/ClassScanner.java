package com.jplus.core.core;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 类扫描器[可自定义]
 *
 * @author huangyong
 * @author Yuanqy
 */
public interface ClassScanner {

	/**
	 * 获取指定包名中的所有类
	 */
	List<Class<?>> getList(String packageName);

	/**
	 * 获取指定包名中指定注解的相关类
	 */
	List<Class<?>> getListByAnnotation(String packageName, Class<? extends Annotation> annotationClass, boolean isRecursive);

	/**
	 * 获取指定包名中指定注解的相关类2
	 */
	List<Class<?>> getListByAnnotation(String packageName, List<Class<? extends Annotation>> annotationClass, boolean isRecursive);

	/**
	 * 获取指定包名中指定父类或接口的相关类
	 */
	List<Class<?>> getListBySuper(String packageName, Class<?> superClass);

	/**
	 * 获取指定包名中指定父类或接口的相关类2
	 */
	List<Class<?>> getListBySuper(String packageName, List<Class<?>> superClass);

	/**
	 * 获取包名下所有的xml文件
	 */
	List<File> getXmlFileList(String packageName);

	/**
	 * 获取包名下所有的接口
	 */
	List<Class<?>> getListIsInterface(String packageName);
}
