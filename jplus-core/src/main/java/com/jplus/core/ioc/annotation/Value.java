package com.jplus.core.ioc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配置文件注入<br>
 * 方式1：@Value("xxx")<br>
 * 方式2：@Value("${xxx}")
 * @author Yuanqy
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Value {
	String value();

}
