package com.jplus.core.bean.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 组件类注解<br>
 * 它是jplus框架的元注解
 * 
 * @author Yuanqy
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Component {
    /**
     * 项目启动后执行该方法
     */
    String initMethod() default "";

    /**
     * 项目关闭前执行该方法
     */
    String destroyMethod() default "";

    /**
     * 执行顺序
     */
    int sort() default 10;
}
