package com.jplus.core.util.verify.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
public @interface V {
	@Retention(RetentionPolicy.RUNTIME)
	@interface IsNotNull {
		String regex() default "\\w+";

		String retmsg() default "字段{}不能为空";

		boolean isNull() default false; // 是否可以为空，true:可以；false:不可以
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsDigits {
		String regex() default "^[0-9]*$";

		String retmsg() default "字段{}不是纯数字";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsFloat {
		String regex() default "^[0-9]*$";

		String retmsg() default "字段{}不是浮点数";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsCN {
		String regex() default "^[\u4e00-\u9fa5]+$";

		String retmsg() default "字段{}不是汉字";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsEmail {
		String regex() default "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

		String retmsg() default "邮箱不正确";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsPhone {
		String regex() default "^[1][0-9]{10}$";

		String retmsg() default "手机号码不正确";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsTel {
		String regex() default "^(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)$";

		String retmsg() default "电话号码不正确";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsIdCardNo {
		String regex() default "^(\\d{6})()?(\\d{4})(\\d{2})(\\d{2})(\\d{3})(\\w)$";

		String retmsg() default "身份证号不正确";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsZipCode {
		String regex() default "^[0-9]{6}$";

		String retmsg() default "邮政编码不正确";

		boolean isNull() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@interface IsPwd {
		String regex() default "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";

		String retmsg() default "密码格式不正确";

		boolean isNull() default true;
	}

	/**
	 * 支持el:/js:/正则 三种表达式
	 * 
	 * @author Yuanqy
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@interface Custom {
		String regex();

		String retmsg() default "未定义";

		boolean isNull() default true;
	}
}
