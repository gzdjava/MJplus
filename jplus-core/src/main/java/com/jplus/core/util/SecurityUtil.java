package com.jplus.core.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.jplus.core.core.CharSet;

/**
 * 加解密工具类
 * 
 * @author Yuanqy
 *
 */
@SuppressWarnings("restriction")
public class SecurityUtil {
	private static Logger logger = LoggerFactory.getLogger(SecurityUtil.class);

	/**
	 * 将byte[] 转换成字符串
	 */
	public static String byte2Hex(byte[] srcBytes) {
		StringBuilder hexRetSB = new StringBuilder();
		for (byte b : srcBytes) {
			String hexString = Integer.toHexString(0x00ff & b);
			hexRetSB.append(hexString.length() == 1 ? 0 : "").append(hexString);
		}
		return hexRetSB.toString();
	}

	/**
	 * 将16进制字符串转为转换成字符串
	 */
	public static byte[] hex2Byte(String source) {
		byte[] sourceBytes = new byte[source.length() / 2];
		for (int i = 0; i < sourceBytes.length; i++) {
			sourceBytes[i] = (byte) Integer.parseInt(source.substring(i * 2, i * 2 + 2), 16);
		}
		return sourceBytes;
	}

	/**
	 * 对字符串加密,加密算法使用MD5,SHA-1,SHA-256,默认使用SHA-256
	 */
	public static String Encrypt(String strSrc, String encName) {
		MessageDigest md = null;
		String strDes = null;
		byte[] bt = strSrc.getBytes();
		try {
			if (encName == null || encName.equals(""))
				encName = "SHA-256";
			md = MessageDigest.getInstance(encName);
			md.update(bt);
			strDes = byte2Hex(md.digest()); // to HexString
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		return strDes;
	}

	// ==Base64加解密==================================================================
	/**
	 * Base64加密
	 */
	public static String Base64Encode(String str) throws UnsupportedEncodingException {
		return new BASE64Encoder().encode(str.getBytes(CharSet.Default));
	}

	/**
	 * 解密
	 */
	public static String Base64Decode(String str) throws UnsupportedEncodingException, IOException {
		str = str.replaceAll(" ", "+");
		return new String(new BASE64Decoder().decodeBuffer(str), CharSet.Default);
	}

	// ==Aes加解密==================================================================
	/**
	 * aes解密-128位
	 */
	public static String AesDecrypt(String encryptContent, String password) {
		try {
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(password.getBytes());
			keyGen.init(128, secureRandom);
			SecretKey secretKey = keyGen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new String(cipher.doFinal(hex2Byte(encryptContent)));
		} catch (Exception e) {
			logger.error("AesDecrypt is Exception", e);
			return null;
		}
	}

	/**
	 * aes加密-128位
	 */
	public static String AesEncrypt(String content, String password) {
		try {
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(password.getBytes());
			keyGen.init(128, secureRandom);
			SecretKey secretKey = keyGen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return byte2Hex(cipher.doFinal(content.getBytes(CharSet.Default)));
		} catch (Exception e) {
			logger.error("AesEncrypt is Exception", e);
			return null;
		}
	}

	public static void main(String[] args) throws Exception {
		String str = "数据加密的基本过程就是对原来为明文的文件或数据按某种算法进行处理，使其成为不可读的一段代码，通常称为“密文”，" + "使其只能在输入相应的密钥之后才能显示出本来内容，通过这样的途径来达到保护数据不被非法人窃取、阅读的目的。 " + "该过程的逆过程为解密，即将该编码信息转化为其原来数据的过程。";
		String PWD = "SecurityUtil.PWD";
		System.out.println("原文:[" + str.length() + "]" + str);
		System.out.println("==MD5===============");
		System.out.println(Encrypt(str, "MD5"));
		System.out.println("==Base64============");
		String strBase64 = Base64Encode(str);
		System.out.println("加密:[" + strBase64.length() + "]" + strBase64);
		System.out.println("解密:" + Base64Decode(strBase64));
		System.out.println("==Aes============");
		String strAes = AesEncrypt(str, PWD);
		System.out.println("加密:[" + strAes.length() + "]" + strAes);
		System.out.println("解密:" + AesDecrypt(strAes, PWD));
	}
}
