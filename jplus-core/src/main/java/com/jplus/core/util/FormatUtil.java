package com.jplus.core.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * 基本工具类
 * 
 * @author Yuanqy
 *
 */
public final class FormatUtil {
	/**
	 * 字符串分隔符
	 */
	public static final String SEPARATOR = String.valueOf((char) 29);

	/** ==============IS Base=================== */
	/** 判断是否为整数(包括负数) */
	public static boolean isNumber(Object arg) {
		return NumberBo(0, toString(arg));
	}

	/** 判断是否为小数(包括整数,包括负数) */
	public static boolean isDecimal(Object arg) {
		return NumberBo(1, toString(arg));
	}

	/** 判断是否为空 ,为空返回true */
	public static boolean isEmpty(Object arg) {
		return toStringTrim(arg).length() == 0 ? true : false;
	}

	/** ==============TO Base=================== */
	/**
	 * Object 转换成 Int 转换失败 返回默认值 0 <br>
	 * 使用:toInt(值,默认值[选填])
	 */
	public static int toInt(Object... args) {
		int def = 0;
		if (args != null) {
			String str = toStringTrim(args[0]);
			// 判断小数情况。舍弃小数位
			int stri = str.indexOf('.');
			str = stri > 0 ? str.substring(0, stri) : str;
			if (args.length > 1)
				def = Integer.parseInt(args[args.length - 1].toString());
			if (isNumber(str))
				return Integer.parseInt(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Long 转换失败 返回默认值 0 <br>
	 * 使用:toLong(值,默认值[选填])
	 */
	public static long toLong(Object... args) {
		Long def = 0L;
		if (args != null) {
			String str = toStringTrim(args[0]);
			if (args.length > 1)
				def = Long.parseLong(args[args.length - 1].toString());
			if (isNumber(str))
				return Long.parseLong(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Double 转换失败 返回默认值 0 <br>
	 * 使用:toDouble(值,默认值[选填])
	 */
	public static double toDouble(Object... args) {
		double def = 0;
		if (args != null) {
			String str = toStringTrim(args[0]);
			if (args.length > 1)
				def = Double.parseDouble(args[args.length - 1].toString());
			if (isDecimal(str))
				return Double.parseDouble(str);
		}
		return def;
	}

	/**
	 * Object 转换成 BigDecimal 转换失败 返回默认值 0 <br>
	 * 使用:toDecimal(值,默认值[选填]) <br>
	 * 【特别注意】: new BigDecimal(Double) 会有误差，得先转String <br>
	 * 【特别注意】: 由于精度问题，不能涉及到其他数据类型 :float,double..浮点数类型<br>
	 * 【特别注意】：涉及到金额的承载及操作，不允许用浮点数！！
	 */
	public static BigDecimal toDecimal(Object... args) {
		BigDecimal def = BigDecimal.ZERO;
		if (args != null) {
			String str = toStringTrim(args[0]);
			if (args.length > 1)
				def = new BigDecimal(args[args.length - 1].toString());
			if (isDecimal(str))
				return new BigDecimal(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Boolean 转换失败 返回默认值 false <br>
	 * 使用:toBoolean(值,默认值[选填])<br>
	 * true=[1,true,"true","ok","yes"]<br>
	 */
	public static boolean toBoolean(Object bo) {
		String bool = toString(bo);
		if (isEmpty(bool) || (!bool.equals("1") && !bool.equalsIgnoreCase("true") && !bool.equalsIgnoreCase("ok") && !bool.equalsIgnoreCase("yes")))
			return false;
		else
			return true;
	}

	/**
	 * Object 转换成 String 为null 返回空字符 <br>
	 * 使用:toString(值,默认值[选填])
	 */
	public static String toString(Object... args) {
		String def = "";
		if (args != null) {
			if (args.length > 1)
				def = toString(args[args.length - 1]);
			Object obj = args[0];
			if (obj == null)
				return def;
			return obj.toString();
		} else {
			return def;
		}
	}

	/**
	 * Object 转换成 String[去除所以空格]; 为null 返回空字符 <br>
	 * 使用:toStringTrim(值,默认值[选填])
	 */
	public static String toStringTrim(Object... args) {
		return trimAll(toString(args));
	}

	/** ==============Other Base=================== */
	/**
	 * 基本数据类型值转换
	 */
	public static Object baseChangeType(Class<?> cla, Object value) {
		String type = cla.getSimpleName();
		if (type.equals("int") || type.equals("Integer")) {
			return FormatUtil.toInt(value);
		} else if (type.equals("long") || type.equals("Long")) {
			return FormatUtil.toLong(value);
		} else if (type.equals("double") || type.equals("Double")) {
			return FormatUtil.toDouble(value);
		} else if (type.equals("boolean") || type.equals("Boolean")) {
			return FormatUtil.toBoolean(value);
		} else if (type.equals("String")) {
			return value;
		} else if (type.equals("BigDecimal")) {
			return FormatUtil.toDecimal(value);
		}
		return null;
	}

	public static String getNowTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	/** 数字左边补0 ,obj:要补0的数， length:补0后的长度 */
	public static String leftPad(Object obj, int len, String def) {
		String param = toStringTrim(obj);
		int l = param.length();
		if (len < l)
			return param;
		return getPadStr(len - l, def) + param;
	}

	/** 获取 len长度的def字符串 */
	public static String getPadStr(int len, String def) {
		String ret = "";
		for (int i = 0; i < len; i++)
			ret += def;
		return ret;
	}

	/** 小数 转 百分数 */
	public static String toPercent(Double str) {
		StringBuffer sb = new StringBuffer(Double.toString(str * 100.0000d));
		return sb.append("%").toString();
	}

	/** 百分数 转 小数 */
	public static Double toPercent2(String str) {
		if (str.charAt(str.length() - 1) == '%')
			return Double.parseDouble(str.substring(0, str.length() - 1)) / 100.0000d;
		return 0d;
	}

	/**
	 * 将byte[] 转换成字符串
	 */
	public static String byte2Hex(byte[] srcBytes) {
		StringBuilder hexRetSB = new StringBuilder();
		for (byte b : srcBytes) {
			String hexString = Integer.toHexString(0x00ff & b);
			hexRetSB.append(hexString.length() == 1 ? 0 : "").append(hexString);
		}
		return hexRetSB.toString();
	}

	/**
	 * 将16进制字符串转为转换成字符串
	 */
	public static byte[] hex2Bytes(String source) {
		byte[] sourceBytes = new byte[source.length() / 2];
		for (int i = 0; i < sourceBytes.length; i++) {
			sourceBytes[i] = (byte) Integer.parseInt(source.substring(i * 2, i * 2 + 2), 16);
		}
		return sourceBytes;
	}

	/** String 转 Money */
	public static String toMoney(Object str, String MoneyType) {
		DecimalFormat df = new DecimalFormat(MoneyType);
		if (isDecimal(str))
			return df.format(toDecimal(str)).toString();
		return df.format(toDecimal("0.00")).toString();
	}

	/** 获取字符串str 左边len位数 */
	public static String getLeft(Object obj, int len) {
		String str = toString(obj);
		if (len <= 0)
			return "";
		if (str.length() <= len)
			return str;
		else
			return str.substring(0, len);
	}

	/** 获取字符串str 右边len位数 */
	public static String getRight(Object obj, int len) {
		String str = toString(obj);
		if (len <= 0)
			return "";
		if (str.length() <= len)
			return str;
		else
			return str.substring(str.length() - len, str.length());
	}

	/**
	 * 首字母变小写
	 */
	public static String firstCharToLowerCase(String str) {
		Character firstChar = str.charAt(0);
		String tail = str.substring(1);
		str = Character.toLowerCase(firstChar) + tail;
		return str;
	}

	/**
	 * 首字母变大写
	 */
	public static String firstCharToUpperCase(String str) {
		Character firstChar = str.charAt(0);
		String tail = str.substring(1);
		str = Character.toUpperCase(firstChar) + tail;
		return str;
	}

	/**
	 * List集合去除重复值 只能用于基本数据类型，。 对象类集合，自己写
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List delMoreList(List list) {
		Set set = new HashSet();
		List newList = new ArrayList();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (set.add(element))
				newList.add(element);
		}
		return newList;
	}

	/**
	 * 字符串格式化<br>
	 * 使用占位符：{}
	 */
	public static String formatParams(String message, Object... params) {
		StringBuffer msg = new StringBuffer();
		String temp = "";
		for (int i = 0; i < params.length + 1; i++) {
			int j = message.indexOf("{}") + 2;
			if (j > 1) {
				temp = message.substring(0, j);
				temp = temp.replaceAll("\\{\\}", FormatUtil.toString(params[i]));
				msg.append(temp);
				message = message.substring(j);
			} else {
				msg.append(message);
				message = "";
			}
		}
		return msg.toString();
	}

	/**
	 * 字符串格式化<br>
	 * 使用占位符：${xxx} <br>
	 * xxx 为你的Map<key>
	 */
	public static String formatParams(String message, Map<String, String> params) {
		for (Map.Entry<String, String> entry : params.entrySet()) {
			message = message.replaceAll("\\$\\{" + entry.getKey() + "\\}", entry.getValue());
		}
		return message;
	}

	/**
	 * 获取随机数
	 * 
	 * @param max
	 *            [0 <= return < max]
	 * @return
	 */
	public static int getRandomInt(int max) {
		return Math.abs(getRandom().nextInt()) % max;
	}

	private static Random random;

	// 获取一个Random单例
	public static Random getRandom() {
		if (random == null) {
			synchronized (FormatUtil.class) {
				if (random == null)
					random = new Random(System.nanoTime());
			}
		}
		return random;
	}

	/** ============== END =================== */
	public final static class MoneyType {
		/** * 保留2位有效数字，整数位每3位逗号隔开 （默认） */
		public static final String DECIMAL = "#,##0.00";
		/** * 保留2位有效数字 */
		public static final String DECIMAL_2 = "0.00";
		/** * 保留4位有效数字 */
		public static final String DECIMAL_4 = "0.0000";
	}

	private static boolean NumberBo(int type, Object obj) {
		if (isEmpty(obj))
			return false;
		int points = 0;
		int chr = 0;
		String str = toString(obj);
		for (int i = str.length(); --i >= 0;) {
			chr = str.charAt(i);
			if (chr < 48 || chr > 57) { // 判断数字
				if (i == 0 && chr == 45) // 判断 - 号
					return true;
				if (i >= 0 && chr == 46 && type == 1) { // 判断 . 号
					++points;
					if (points <= 1)
						continue;
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * List拆分成多个
	 */
	public static <T> List<List<T>> splitList(List<T> targe, int size) {
		List<List<T>> listArr = new ArrayList<List<T>>();
		// 获取被拆分的数组个数
		int arrSize = targe.size() % size == 0 ? targe.size() / size : targe.size() / size + 1;
		for (int i = 0; i < arrSize; i++) {
			List<T> sub = new ArrayList<T>();
			// 把指定索引数据放入到list中
			for (int j = i * size; j <= size * (i + 1) - 1; j++) {
				if (j <= targe.size() - 1) {
					sub.add(targe.get(j));
				}
			}
			listArr.add(sub);
		}
		return listArr;
	}

	/**
	 * 去除所有空白字符
	 */
	public static String trimAll(String str) {
		return str.replaceAll("\\s*", "");
	}

	/** 格式化路径中的空白字符和斜杠 */
	public static String formatPath(String path) {
		path = path.replaceAll("\\\\", "/");
		path = path.replaceAll("/+", "/");
		if ('/' == (path.charAt(path.length() - 1)))
			path = path.substring(0, path.length() - 1);
		path = FormatUtil.trimAll(path);
		return path;
	}

	/**
	 * 得到当前系统前多少分钟
	 */
	public static String beforeMinuteAgo(int fen) {
		long currentTime = ClockUtil.CLOCK.now() - fen * 60 * 1000;
		Date date = new Date(currentTime);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(date.getTime());
	}
}