package com.jplus.core.util;

/**
 * 起因：System.currentTimeMillis原理是调用native方法，涉及到了jvm底层硬件交互，可能出现延迟的情况，照成业务流程耗时加大。
 * 
 * @author Yuanqy
 *
 */
public class ClockUtil {
    private long rate = 0;// 频率
    private volatile long now = 0;// 当前时间

    private ClockUtil(long rate) {
	this.rate = rate;
	this.now = System.currentTimeMillis();
	start();
    }

    private void start() {
	new Thread(new Runnable() {
	    @Override
	    public void run() {
		try {
		    Thread.sleep(rate);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		now = System.currentTimeMillis();
	    }
	}).start();
    }

    public long now() {
	return now;
    }

    public static final ClockUtil CLOCK = new ClockUtil(10);
}