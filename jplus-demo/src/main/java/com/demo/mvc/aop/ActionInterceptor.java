package com.demo.mvc.aop;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.aop.AspectProxy;
import com.jplus.core.aop.annotation.Aspect;
import com.jplus.core.mvc.DataContext;
import com.jplus.core.mvc.annotation.Controller;

/**
 * AOP拦截器演示<br/>
 * 测试拦截指定包名下所有的Action
 * 
 * @author Yuanqy
 */
@Aspect(pkg = "com.demo.mvc.action", annotation = Controller.class)
public class ActionInterceptor extends AspectProxy {
	private Logger logger = LoggerFactory.getLogger(getClass());

	ThreadLocal<Long> curTimes = new ThreadLocal<Long>();

	@Override
	public void before(Class<?> cls, Method method, Object[] params) throws Throwable {
		curTimes.set(System.currentTimeMillis());
		logger.info("===Is new RequestURI：" + DataContext.getRequest().getRequestURI());
		logger.info("===Action:{}.{}", cls.getName(), method.getName());
	}

	@Override
	public void after(Class<?> cls, Method method, Object[] params, Object result) throws Throwable {
		logger.info("===Action is complete,time：{}ms \n", (System.currentTimeMillis() - curTimes.get()));
		curTimes.remove();
	}

}
