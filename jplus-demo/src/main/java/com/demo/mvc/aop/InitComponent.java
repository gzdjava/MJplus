package com.demo.mvc.aop;

import com.jplus.core.bean.annotation.Component;

/**
 * 属于Bean扫描注解，任何需要加入IOC的组件类，都 可以添加该注解。<br>
 * 该注解提供两个参数[都非必填项]：
 * initMethod：填入当前类的方法名，项目启动后会执行该方法
 * destroyMethod：填入当前类的方法名，项目注销前会执行该方法
 */
@Component(initMethod = "init", destroyMethod = "destroy")
public class InitComponent {

	public void init() {
		System.err.println("== 项目启动后会执行该方法 ==");
	}

	public void destroy() {
		System.err.println("== 项目注销前会执行该方法 ==");
	}
}
