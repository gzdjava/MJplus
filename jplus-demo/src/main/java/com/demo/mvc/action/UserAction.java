package com.demo.mvc.action;

import com.demo.mvc.bean.User;
import com.demo.mvc.server.UserService;
import com.jplus.core.ioc.annotation.Autowired;
import com.jplus.core.mvc.WebUtil;
import com.jplus.core.mvc.annotation.Controller;
import com.jplus.core.mvc.annotation.Param;
import com.jplus.core.mvc.annotation.Request;
import com.jplus.core.mvc.bean.Result;
import com.jplus.core.util.verify.VerifyUtil;

@Controller
@Request("user/")
public class UserAction {

	private @Autowired UserService userService;

	@Request.All("info")
	private void info() {
		WebUtil.writeHTML("This is Page info");
	}

	@Request.Get("{id}")
	private void getUser(int id) {
		WebUtil.writeHTML("This is a get user info by id,id:" + id);
	}

	/**
	 * 发现和上面getUser的方法的区别了么，
	 * 上面直接取值，使用的下标取值，取的restful请求中的占位符{xx}中的值，然后按下标依次取，支持类型
	 * ：int/long/Double/BigDecimal/String
	 * 下面通过@Param取值，使用名称key取值，可以取restful/get/post
	 * ...等各种方式提交的值，支持类型：int/long/Double/BigDecimal/String
	 */
	@Request.Get("{id}")
	private void getUser2(@Param("id") int userId) {
		WebUtil.writeHTML("This is a get user info by id,id:" + userId);
	}

	@Request.Post("add")
	private Result addUser(User form) {
		Result res = VerifyUtil.doVerify(form);
		if (res.IsOK())
			return userService.add(form);
		return res;
	}

	@Request.Put("edit")
	private void editUser() {
		WebUtil.writeHTML("This is edit Page");
	}

	@Request.Delete("{id}")
	private void delUser(int id) {
		WebUtil.writeHTML("This is del Page,user id is :" + id);
	}
}
