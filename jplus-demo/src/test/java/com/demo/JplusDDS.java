package com.demo;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.db.dynamic.DataSourceHolder;
import com.jplus.core.db.dynamic.DynamicDataSource;
import com.jplus.core.junit.JplusJunit4Runner;
import com.jplus.core.junit.annotation.ContextConfiguration;
import com.jplus.core.util.JsonUtil;
import com.jplus.orm.c3p0.C3p0Plugin;

/**
 * JplusJunit4 测试Jplus-core编程式多数据源切换
 */
@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:app.properties")
public class JplusDDS {

	@Test
	public void test() {
		JdbcTemplate jt = null;
		try {
			// 1.创建N个数据源，可选用C3p0，Druid...都可以
			DataSource ds1 = new C3p0Plugin("jdbc:mysql://139.196.18.60:3306", "root", "password", "com.mysql.jdbc.Driver").getDataSource();
			DataSource ds2 = new C3p0Plugin("jdbc:mysql://192.168.1.16:3306", "rd_user", "password", "com.mysql.jdbc.Driver").getDataSource();
			// 2.将N个数据源放入DynamicDataSource中，并定义key名称
			Map<String, DataSource> map = new HashMap<String, DataSource>();
			map.put("readone", ds1);
			map.put("readtwo", ds2);
			DynamicDataSource dds = new DynamicDataSource(map);
			// 3.创建一个JdbcTemplate，使用数据源为DynamicDataSource
			jt = new JdbcTemplate(dds);

			// 4.循环测试数据源切换[ps.我事先准备了两个不同版本的mysql]
			for (int i = 0; i < 3; i++) {
				if (i == 2)
					jt.openTransaction(); // 最后一次开启事务
				DataSourceHolder.setDbType("readone");
				System.err.println("[切换1]:" + JsonUtil.toJson(jt.query("SHOW VARIABLES LIKE 'version';", new Object[] {})));
				DataSourceHolder.setDbType("readtwo");
				System.err.println("[切换2]:" + JsonUtil.toJson(jt.query("SHOW VARIABLES LIKE 'version';", new Object[] {})));
				if (i == 2)
					jt.commit(); // 提交事务
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (jt != null)
				jt.rollback(); // 回滚事务
		}

	}
}
