package com.demo;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.demo.mvc.bean.User;
import com.demo.mvc.iface.IIocDemo;
import com.demo.mvc.iface.impl.IocDemoImpl;
import com.jplus.core.ioc.annotation.Autowired;
import com.jplus.core.ioc.annotation.Value;
import com.jplus.core.junit.JplusJunit4Runner;
import com.jplus.core.junit.annotation.ContextConfiguration;

/**
 * JplusJunit4 测试框架IOC功能
 */
@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:app.properties")
public class JunitIOC {

	private @Autowired("iocDemoImpl") IIocDemo ioc3;
	private @Autowired IocDemoImpl ioc2;

	private @Value("app.scan.pkg") String scanPkg;

	@Test
	public void test() throws InterruptedException {
		System.out.println("添加用户：" + ioc3.addUser(new User(1001, "Jplus")));
		System.out.println("获取用户：" + ioc2.getUser(1001));
		System.out.println("app.scan.pkg=" + scanPkg);
		
		Thread.currentThread().join();
	}
}
