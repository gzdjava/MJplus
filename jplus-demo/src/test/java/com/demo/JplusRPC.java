package com.demo;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.jplus.core.junit.JplusJunit4Runner;
import com.jplus.core.junit.annotation.ContextConfiguration;

/**
 * JplusJunit4 测试Jplus-rpc
 */
@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:rpc.properties")
public class JplusRPC {

	@Test
	public void test() {

	}
}
