package com.demo;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.jplus.core.InstanceFactory;
import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.junit.JplusJunit4Runner;
import com.jplus.core.junit.annotation.ContextConfiguration;
import com.jplus.core.util.JsonUtil;

/**
 * JplusJunit4 测试框架ORM功能
 */
@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:orm.properties")
public class JplusOrm {
	
	@Test
	public void testJdbcTemplete() throws InterruptedException {
		try {
			JdbcTemplate jdbc = InstanceFactory.getDataSourceFactory().getJdbcTemplate();
			List<Map<String, Object>> list = jdbc.queryList("SHOW PROCESSLIST", null);
			for (Map<String, Object> map : list) {
				System.err.println(JsonUtil.toJson(map));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		Thread.currentThread().join();
	}
}
