<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Jplus loading...</title>

<link href="${ctx}/resource/css/base.css?v=${v}" rel="stylesheet" type="text/css" />
<link href="${ctx}/resource/css/grid.css?v=${v}" rel="stylesheet" type="text/css" />
<link href="${ctx}/resource/css/tips.css?v=${v}" rel="stylesheet" type="text/css" />
<link href="${ctx}/resource/css/public.css?v=${v}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/resource/js/jquery.min.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/baseJs.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/common.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/tips.js?v=${v}"></script>
<script type="text/javascript">
	//window.location.href = "${ctx}/home";
</script>
<style type="text/css">
	*{color: #343434;}
	.PW{width: 1000px;margin: auto;}
	.TOP{color:#666666;}
	
/* 	--- */
</style>
</head>
<body>
	<div class="TOP">
		<div class="PW pt30 tc">
			 <div>
				 <button>网站维护中，近期开放，请稍候~</button>
			 </div>
			 <div class="mt100 tl">
			 	<table>
			 		<tr><th colspan="2">Jplus框架文档：</th></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-core.html">jplus-core</a></td><td>jplus框架核心，集成MVC,AOP,IOC,单元测试,JdebTemplete 等功能~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-orm.html">jplus-orm</a></td><td>jplus数据库框架，集成Mybatis,C3P0,Druid,拥有事务管理,支持多数据源,sql日志跟踪~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-rpc.html">jplus-rpc</a></td><td>netty4+zookeeper,支持[netty,scokey,http[跨语言]]三种通讯协议~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-j2cache.html">jplus-j2cache</a></td><td>jplus二级缓存框架，集成echche+redis，拥有数据同步，保证分布式应用数据一致性~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-job.html">jplus-job</a></td><td>jplus作业框架，集成Quartz，拥有动态作业配置功能，及其作业监控~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-mq.html">jplus-mq</a></td><td>jplus消息框架，集成ActiveMq-jms~</td></tr>
			 	</table>
			 </div>
		</div>
	</div>
</body>
</html>