
function showAlert(options) {
	var dfoption = {
		width : 600,// 弹出框宽度
		height : 0,// 弹出框高度，
		title : '',
		msg : 'This is msg',
		msgType : '',// jquery
		btnCannelVal : 'Cannel',
		btnOkVal : 'Let\'s do it',
		btnOkFunc:function(){alert("This click default OK");}
	};
	options = $.extend(dfoption, options);
	if (options.width > $(window).width())
		options.width = $(window).width() - 10;
	var $dialog=$("#Dialog");
	var $close=$("#dialogClose");
	var $title=$("#dialogTop");
	var $context=$("#dialogCent");
	var $footer=$("#dialogBot")
	
	$title.html(options.title);
	$context.html(options.msgType=="jquery"?$(options.msg).html():options.msg);
	$footer.find("button").hide();
	if (options.btnOkVal.length>0){
		$footer.find(".btnOk").text(options.btnOkVal).show();
		$footer.find(".btnOk").unbind("click").bind("click",options.btnOkFunc);
	}
	if (options.btnCannelVal.length>0){
		$footer.find(".btnCannel").text(options.btnCannelVal).show();
		$footer.find(".btnCannel").unbind("click").bind("click",function(){$dialog.hide()});
	}
	$close.find("button").unbind("click").bind("click",function(){$dialog.hide()});
	$dialog.show();
	// ===;;
}