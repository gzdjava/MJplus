//
//		tips  提示插件
//
//hoverTips();		悬浮显示
//clickTips();		单击显示
//addTips(id,msg);	添加显示(标识号,提示信息)
//delTips(id);		删除显示(标识号)

//==主题==
var tipsTheme="yellow";
var tipsTheme="red";
var tipsTheme="black";
// $obj 是jq对象
function Error($obj,alias,msg){
	$obj.addTips(alias,msg,"red");
	setTimeout(function(){
		$obj.delTips(alias);
	},4000);
	return false;
}
//==扫描加载页面需要提示的地方
$(function(){
	initTips();
})
function initTips(){
	$("body *").each(function(){
		var tipsH=$(this).attr("tipsH");
		if(typeof(tipsH)!="undefined") $(this).hoverTips()
		var tipsC=$(this).attr("tipsC");
		if(typeof(tipsC)!="undefined") $(this).clickTips()
	})
}


$.fn.extend({
	hoverTips : function (){
		var _self = $(this);
		var content = _self.attr("tipsH");
		var htmlDom = tipsGetCreateDom(tipsTheme);
		_self.on("mouseover",function(){
			tipsSetLT(_self,htmlDom,content);
			htmlDom.stop().animate({ "opacity" : 1},300);
		});
		_self.on("mouseout",function(){
			var top = parseInt(htmlDom.css("top"));
			htmlDom.stop().animate({ "opacity" : 0},300,function(){
				htmlDom.remove();
			});
		});
	},
	clickTips : function (){
		var _self = $(this);
		var content = _self.attr("tipsC");
		var htmlDom = tipsGetCreateDom(tipsTheme);
		_self.on("click",function(){
			tipsSetLT(_self,htmlDom,content);
			htmlDom.stop().animate({ "opacity" : 1},300, function(){
				setTimeout(function(){
					htmlDom.stop().animate({"top":top - 10 ,"opacity" : 0},300,function(){
						htmlDom.remove();
					})
				},2000)
			});
		})
	},
	addTips:function(id,msg,type){
		if(typeof(type)=="undefined")
			type=tipsTheme;
		var _self = $(this);
		var content = msg;
		var htmlDom =tipsGetCreateDom(type,id);
		tipsSetLT(_self,htmlDom,content);
		htmlDom.stop().animate({ "opacity" : 1},300, function(){ });
	},
	delTips:function(id){
		//alert(id);
		var htmlDom=$("body").find(".tips_"+id);
		htmlDom.stop().animate({ "opacity" : 0},300,function(){
				htmlDom.remove();
		});
	}
});

function tipsSetLT(_self,htmlDom,content){
	htmlDom.find("p.content").html( content );
	_self.before( htmlDom );
//	var left = _self.offset().left - htmlDom.outerWidth()/2 + _self.outerWidth()/2;  //center
	//var left = _self.offset().left ;  //left
//	var left=_self.offset().left+_self.outerWidth()-htmlDom.outerWidth();//right
	//var top = _self.offset().top - htmlDom.outerHeight() - parseInt(htmlDom.find(".triangle-front").css("border-left-width"));
	//var top = _self.offset().top - htmlDom.outerHeight();
	htmlDom.css({"margin-left":"0px","margin-top":-htmlDom.outerHeight()-10+"px","display":"block"});
	
	htmlDom.find("p.triangle-front,p.triangle-back").css("left",(htmlDom.width()>_self.width()?_self.width()/2:10)+"px");
}
function tipsGetCreateDom(_tipsTheme,id){
	return $("<div class='tips tips_"+id+"'>")
	.addClass(_tipsTheme)
	.html("<p class='content'></p>"
			+ "<p class='triangle-front'></p>"
			+ "<p class='triangle-back'></p>");
}
