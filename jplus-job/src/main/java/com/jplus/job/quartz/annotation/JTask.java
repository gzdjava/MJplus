package com.jplus.job.quartz.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;

/**
 * Quartz定时器注解
 * 
 * @author Yuanqy
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component	//用于被框架扫描
public @interface JTask {
	String jobName() default "默认作业名称";

	String jobGroup() default "默认作业分组";

	String triggerName() default "默认触发器名称";

	String triggerGroup() default "默认触发器分组";

	/** cron 表达式 
	 *<pre> @JTask(cron = "${task.cron}") //支持读取配置信息</pre>
	 *<pre> @JTask(cron = "0 0/10 * * * ?") //正常的时间配置事例</pre>
	 * */
	String cron();
}
