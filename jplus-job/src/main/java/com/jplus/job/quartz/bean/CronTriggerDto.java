package com.jplus.job.quartz.bean;

/**
 * CronTriggerDto
 * @author Yuanqy
 *
 */
public class CronTriggerDto {
	private String name;
	private String group;
	private String cron;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCron() {
		return cron;
	}

	/**
	 * Cron表达式
	 * 
	 * @param cron
	 */
	public void setCron(String cron) {
		this.cron = cron;
	}

	public CronTriggerDto(String name, String group, String cron) {
		super();
		this.name = name;
		this.group = group;
		this.cron = cron;
	}
	public CronTriggerDto(String name, String cron) {
		super();
		this.name = name;
		this.group = "default_trigger_group";
		this.cron = cron; 
	}
}
