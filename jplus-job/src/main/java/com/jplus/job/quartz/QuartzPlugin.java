package com.jplus.job.quartz;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.bean.BeanHandle;
import com.jplus.core.plugin.Plugin;
import com.jplus.job.quartz.annotation.JTask;

/**
 * Quartz是一个大名鼎鼎的Java版开源定时调度器，功能强悍，使用方便。 一、核心概念
 * Quartz的原理不是很复杂，只要搞明白几个概念，然后知道如何去启动和关闭一个调度程序即可。 1、Job
 * 表示一个工作，要执行的具体内容。此接口中只有一个方法 void execute(JobExecutionContext context)
 * 2、JobDetail
 * JobDetail表示一个具体的可执行的调度程序，Job是这个可执行程调度程序所要执行的内容，另外JobDetail还包含了这个任务调度的方案和策略。
 * 3、Trigger代表一个调度参数的配置，什么时候去调。
 * 4、Scheduler代表一个调度容器，一个调度容器中可以注册多个JobDetail和Trigger。
 * 当Trigger与JobDetail组合，就可以被Scheduler容器调度了。
 * 
 * @author Yuanqy
 *
 */

public class QuartzPlugin implements Plugin {

	private Logger logger = LoggerFactory.getLogger(getClass());
	private static Set<Class<?>> jobSet = new HashSet<Class<?>>();

	@Override
	public void init() {
		for (Class<?> cla : BeanHandle.beanSet) {
			if (cla.isAnnotationPresent(JTask.class)) {
				jobSet.add(cla);
				logger.info("\t\t[quartz][{}]{}", cla.getAnnotation(JTask.class).cron(), cla.getName());
			}
		}
	}

	@Override
	public void destroy() {
		BeanHandle.getBean(QuartzLoad.class).destroy();
	}

	public static Set<Class<?>> getJobSet() {
		return jobSet;
	}
}
