package com.jplus.j2cache.cache.listener;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.j2cache.cache.J2CacheManager;

/**
 * 侦听缓存中的某个记录超时
 * 
 * @author winterlau
 */
public class CacheListener {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private J2CacheManager manger;

	public CacheListener(J2CacheManager manger) {
		this.manger = manger;
	}

	/**
	 * 当缓存中的某个对象超时被清除的时候触发
	 */
	@SuppressWarnings("rawtypes")
	public void notifyElementExpired(byte LEVEL, String region, Object key) {
		logger.debug("Cache data expired, region=" + region + ",key=" + key);
		// 删除二级缓存
		if (key instanceof List)
			manger.batchEvict(LEVEL, region, (List) key);
		else
			manger.evict(LEVEL, region, key);
		// 发送广播,表示由1级缓存发起，清空其他机器上的1级缓存
		if (LEVEL == 1)
			manger._sendEvictCmd(region, key);
	}
}
