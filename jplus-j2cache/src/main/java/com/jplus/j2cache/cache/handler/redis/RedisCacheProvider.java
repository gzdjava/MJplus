package com.jplus.j2cache.cache.handler.redis;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.util.FormatUtil;
import com.jplus.core.util.JsonUtil;
import com.jplus.j2cache.cache.iface.Cache;
import com.jplus.j2cache.cache.iface.CacheProvider;
import com.jplus.j2cache.cache.listener.CacheListener;
import com.jplus.j2cache.fault.CacheException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis 缓存实现
 * 
 * @author Winter Lau
 * @author wendal
 */
public class RedisCacheProvider implements CacheProvider {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private static JedisPool pool;
	protected ConcurrentHashMap<String, RedisCache> caches = new ConcurrentHashMap<>();

	public String name() {
		return "redis";
	}

	public static Jedis getResource() {
		return pool.getResource();
	}

	@Override
	public Cache buildCache(String regionName, boolean autoCreate, CacheListener listener) throws CacheException {
		// 虽然这个实现在并发时有概率出现同一各regionName返回不同的实例
		// 但返回的实例一次性使用,所以加锁了并没有增加收益
		RedisCache cache = caches.get(regionName);
		if (cache == null) {
			cache = new RedisCache(regionName, pool, listener);
			caches.put(regionName, cache);
		}
		return cache;
	}

	@Override
	public void start(Properties props) throws CacheException {
		JedisPoolConfig config = new JedisPoolConfig();
		String prefix = "redis.";
		String host = getProperty(props, prefix + "host", "127.0.0.1");
		String password = props.getProperty(prefix + "password", null);

		int port = getProperty(props, prefix + "port", 6379);
		int timeout = getProperty(props, prefix + "timeout", 5000);
		int database = getProperty(props, prefix + "database", 0);

		config.setBlockWhenExhausted(getProperty(props, prefix + "blockWhenExhausted", true));
		config.setMaxIdle(getProperty(props, prefix + "maxIdle", 10));
		config.setMinIdle(getProperty(props, prefix + "minIdle", 5));
		// config.setMaxActive(getProperty(props, prefix+"maxActive", 50));
		config.setMaxTotal(getProperty(props, prefix + "maxTotal", 10000));
		config.setMaxWaitMillis(getProperty(props, prefix + "maxWait", 100));
		config.setTestWhileIdle(getProperty(props, prefix + "testWhileIdle", false));
		config.setTestOnBorrow(getProperty(props, prefix + "testOnBorrow", true));
		config.setTestOnReturn(getProperty(props, prefix + "testOnReturn", false));
		config.setNumTestsPerEvictionRun(getProperty(props, prefix + "numTestsPerEvictionRun", 10));
		config.setMinEvictableIdleTimeMillis(getProperty(props, prefix + "minEvictableIdleTimeMillis", 1000));
		config.setSoftMinEvictableIdleTimeMillis(getProperty(props, prefix + "softMinEvictableIdleTimeMillis", 10));
		config.setTimeBetweenEvictionRunsMillis(getProperty(props, prefix + "timeBetweenEvictionRunsMillis", 10));
		config.setLifo(getProperty(props, prefix + "lifo", false));
		logger.debug("==>JedisPoolConfig:" + JsonUtil.toJson(config));
		logger.debug("==>JedisPoolUser:", host, port, timeout, password, database);
		pool = new JedisPool(config, host, port, timeout, password, database);
	}

	@Override
	public void stop() {
		pool.destroy();
		caches.clear();
	}

	private static String getProperty(Properties props, String key, String defaultValue) {
		return props.getProperty(key, defaultValue).trim();
	}

	private static int getProperty(Properties props, String key, int defaultValue) {
		try {
			return Integer.parseInt(props.getProperty(key, String.valueOf(defaultValue)).trim());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	private static boolean getProperty(Properties props, String key, boolean defaultValue) {
		return "true".equalsIgnoreCase(props.getProperty(key, String.valueOf(defaultValue)).trim());
	}
}
