package com.jplus.j2cache.cache;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.j2cache.J2Cache;
import com.jplus.j2cache.cache.handler.ecache.EhCacheProvider;
import com.jplus.j2cache.cache.handler.empty.NullCacheProvider;
import com.jplus.j2cache.cache.handler.redis.RedisCacheProvider;
import com.jplus.j2cache.cache.iface.Cache;
import com.jplus.j2cache.cache.iface.CacheProvider;
import com.jplus.j2cache.cache.listener.CacheListener;
import com.jplus.j2cache.fault.CacheException;

/**
 * 缓存管理器
 * 
 * @author Winter Lau
 */
public class J2CacheManager {

	private final Logger log = LoggerFactory.getLogger(J2CacheManager.class);

	private CacheProvider l1_provider;
	private CacheProvider l2_provider;
	private CacheListener listener;

	/**
	 * Initialize Cache Provider
	 */
	public J2CacheManager() {
		try {
			Properties prop = J2Cache.getConfig();
			this.l1_provider = getProviderInstance(prop.getProperty("cache.L1.provider_class", "ehcache"));
			this.l1_provider.start(prop);
			log.info("Using L1 CacheProvider : " + l1_provider.getClass().getName());

			this.l2_provider = getProviderInstance(prop.getProperty("cache.L2.provider_class", "redis"));
			this.l2_provider.start(prop);
			log.info("Using L2 CacheProvider : " + l2_provider.getClass().getName());

			this.listener = new CacheListener(this);
		} catch (Exception e) {
			throw new CacheException("Unabled to initialize cache providers", e);
		}
	}

	private final CacheProvider getProviderInstance(String value) throws Exception {
		if ("ehcache".equalsIgnoreCase(value))
			return new EhCacheProvider();
		if ("redis".equalsIgnoreCase(value))
			return new RedisCacheProvider();
		if ("none".equalsIgnoreCase(value))
			return new NullCacheProvider();
		return (CacheProvider) Class.forName(value).newInstance();
	}

	private final Cache _GetCache(int level, String cache_name, boolean autoCreate) {
		return ((level == 1) ? l1_provider : l2_provider).buildCache(cache_name, autoCreate, listener);
	}

	/**
	 * 获取缓存中的数据
	 */
	public final Object get(int level, String name, Object key) {
		// System.out.println("GET1 => " + name+":"+key);
		if (name != null && key != null) {
			Cache cache = _GetCache(level, name, false);
			if (cache != null)
				return cache.get(key);
		}
		return null;
	}

	/**
	 * 获取缓存中的数据
	 */
	@SuppressWarnings("unchecked")
	public final <T> T get(int level, Class<T> resultClass, String name, Object key) {
		// System.out.println("GET2 => " + name+":"+key);
		if (name != null && key != null) {
			Cache cache = _GetCache(level, name, false);
			if (cache != null)
				return (T) cache.get(key);
		}
		return null;
	}

	/**
	 * 写入缓存
	 */
	public final void set(int level, String name, Object key, Object value) {
		// System.out.println("SET => " + name+":"+key+"="+value);
		if (name != null && key != null && value != null) {
			Cache cache = _GetCache(level, name, true);
			if (cache != null)
				cache.put(key, value);
		}
	}

	/**
	 * 清除缓存中的某个数据
	 */
	public final void evict(int level, String name, Object key) {
		// batchEvict(level, name, java.util.Arrays.asList(key));
		if (name != null && key != null) {
			Cache cache = _GetCache(level, name, false);
			if (cache != null)
				cache.evict(key);
		}
	}

	/**
	 * 批量删除缓存中的一些数据
	 */
	@SuppressWarnings("rawtypes")
	public final void batchEvict(int level, String name, List keys) {
		if (name != null && keys != null && keys.size() > 0) {
			Cache cache = _GetCache(level, name, false);
			if (cache != null)
				cache.evict(keys);
		}
	}

	/**
	 * Clear the cache
	 */
	public final void clear(int level, String name) throws CacheException {
		Cache cache = _GetCache(level, name, false);
		if (cache != null)
			cache.clear();
	}

	/**
	 * list cache keys
	 */
	@SuppressWarnings("rawtypes")
	public final List keys(int level, String name) throws CacheException {
		Cache cache = _GetCache(level, name, false);
		return (cache != null) ? cache.keys() : null;
	}

	public final void shutdown(int level) {
		((level == 1) ? l1_provider : l2_provider).stop();
	}

	// ================================================
	// 发布清空命令，通过二级缓存的广播，清空一级
	public void _sendEvictCmd(String region, Object key) {
		Cache cache = _GetCache(Cache.LEVEL_2, region, false);
		cache.sendEvictCmd(region, key);
	}
}
