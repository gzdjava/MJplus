package com.jplus.j2cache;

import com.jplus.core.plugin.Plugin;

/**
 * ECachePlugin
 * 
 * @author yuanqy
 *
 */
public class CachePlugin implements Plugin {

	public static boolean CACHESTATE = false;

	@Override
	public void init() {
		CACHESTATE = true;
		J2Cache.set("J2Cache", "init", "hello world");
	}

	@Override
	public void destroy() {
		if (CACHESTATE)
			J2Cache.close();
	}
}
