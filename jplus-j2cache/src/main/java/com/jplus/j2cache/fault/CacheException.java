package com.jplus.j2cache.fault;

/**
 * 缓存相关的异常
 * 
 * @author liudong
 */
public class CacheException extends RuntimeException {

	private static final long serialVersionUID = -1463710225530548524L;

	public CacheException(String s) {
		super(s);
	}

	public CacheException(String s, Throwable e) {
		super(s, e);
	}

	public CacheException(Throwable e) {
		super(e);
	}

}
