package com.jplus.j2cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 删除缓存
 * @author Yuanqy
 *
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CacheEvict {
	/**
	 * 缓存名称，不填默认为:项目名
	 */
	String value() default "";

	/**
	 * 缓存Key,默认:为空字符串，表示清空当前value()下所有key()
	 */
	String key() default "";
}
