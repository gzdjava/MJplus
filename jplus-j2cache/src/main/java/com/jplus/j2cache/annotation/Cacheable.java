package com.jplus.j2cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 添加缓存
 * @author Yuanqy
 *
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cacheable {
	/**
	 * 缓存名称，默认为: 项目名
	 */
	String value() default "";

	/**
	 * 缓存Key,默认为:类名_方法名_参数MD5
	 */
	String key() default "";
}
